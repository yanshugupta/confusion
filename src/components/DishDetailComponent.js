import React, {Component} from 'react'
import { Card, CardImg, CardTitle, CardBody, Row, CardText, BreadcrumbItem, Breadcrumb, Button, Modal, ModalHeader, ModalBody, Label } from 'reactstrap'
import { Link } from 'react-router-dom'
import { LocalForm, Control, Errors } from 'react-redux-form'
import { Loading } from './LoadingComponent';
import { baseUrl } from '../shared/baseUrl';
import { FadeTransform, Fade, Stagger } from 'react-animation-components';


const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => (val) && (val.length >= len);

const RenderComments = ({comments}) => {
    if(comments != null){
        const DishComments = comments.map(comment => {
            return(
                <Stagger in>
                    <Fade in>
                        <div key={comment.id}>
                            <p>{comment.comment}</p>
                            <p>--{comment.author}, {new Intl.DateTimeFormat('en-us', {year: 'numeric', month: 'short', day: '2-digit'}).format(new Date(Date.parse(comment.date)))}</p>
                        </div>
                    </Fade>
                </Stagger>
            )
        });
        return (DishComments)
    }
    return (<div></div>)   
}
 class CommentForm extends Component {
    constructor(props){
        super();
        this.state = {
            isModalOpen: false
        }
        this.toggleModal = this.toggleModal.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleSubmit(values){
        console.log("Current state is"+ JSON.stringify(values))
        console.log(this.state.dishId+"|"+ values.rating+"|"+ values.name+"|"+ values.comment)
        this.props.postComment(this.props.dishId, values.rating, values.name, values.comment);
    }

    toggleModal(){
        this.setState({
            isModalOpen: !this.state.isModalOpen
        })
    }
    render(){
        return(
            <div>
                <Button outline onClick={this.toggleModal}>
                    <span className="fa fa-pencil fa-fw-2x"> Submit Comment</span>
                </Button>
                <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                    <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
                    <ModalBody>
                        <LocalForm onSubmit={(values) => this.handleSubmit(values)}>
                            <Row className="form-group">
                                <Label htmlFor="rating">Rating</Label>
                                <Control.select model=".rating" id="rating" name="rating"
                                    className="form-control"
                                    >
                                    <option value="1" selected>1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </Control.select>
                            </Row>
                            <Row className="form-group">
                                <Label htmlFor="name">Your Name</Label>
                                <Control.text model=".name" id="name" name="name"
                                    className="form-control" placeholder="Your Name"
                                    validators={{
                                        required,
                                        minLength: minLength(3),
                                        maxLength: maxLength(15)
                                    }}
                                    />
                                <Errors className="text-danger"
                                    model=".name"
                                    show="touched"
                                    messages={{
                                        required: 'Required',
                                        minLength: 'Must be greater then 2 characters',
                                        maxLength: 'Must be less than 15 characters'
                                    }}/>
                            </Row>
                            <Row className="form-group">
                                <Label htmlFor="comment">Commnet</Label>
                                <Control.textarea model=".comment" id="comment" name="commnet"
                                    className="form-control"
                                    /> 
                            </Row>
                            <br/>
                            <Button type="submit" value="submit" className="bg-primary">Submit</Button>
                        </LocalForm>
                    </ModalBody>
                </Modal>
            </div>
        )
    }
 }

class DishDetail extends Component {
    
    constructor(props){
        super();
    }

    render(){
        const selectedDish = this.props.selectedDish;
        if (this.props.isLoading) {
            return(
                <div className="container">
                    <div className="row">            
                        <Loading />
                    </div>
                </div>
            );
        }
        else if (this.props.errMess) {
            return(
                <div className="container">
                    <div className="row">            
                        <h4>{this.props.errMess}</h4>
                    </div>
                </div>
            );
        }
        else if(selectedDish != null){
            
            return(
                <div className="container">
                    <div className="row">
                        <Breadcrumb>
                            <BreadcrumbItem><Link to='/menu'>Menu</Link></BreadcrumbItem>
                            <BreadcrumbItem active>{selectedDish.name}</BreadcrumbItem>
                        </Breadcrumb>
                        <div className="col-12">
                            <h3>{selectedDish.name}</h3>
                            <hr/>
                        </div>
                    </div> 
                    <div className="row">
                        <div key="dishDetails" className="col-12 col-md-5">
                            <br></br>
                            <FadeTransform in
                                transformProps={{
                                    exitTransform: 'scale(0.5) translateY(-50%)'
                                }}>
                                <Card>
                                    <CardImg width="100%" src={baseUrl + selectedDish.image} alt={selectedDish.name} />
                                    <CardBody>
                                        <CardTitle>{selectedDish.name}</CardTitle>
                                        <CardText>{selectedDish.description}</CardText>
                                    </CardBody>
                                </Card>
                            </FadeTransform>
                            <br></br>
                        </div>
                        <div key={selectedDish.id} className="col-12 col-md-5">
                            <h1>comments</h1>
                            <RenderComments comments={this.props.comments} />
                            <CommentForm dishId={selectedDish.id} postComment={this.props.postComment}/>
                        </div>
                    </div>
                </div>
            )
        }
        else{
            return(
                <div></div>
            )
        }
    }
}

export default DishDetail;